module.exports = {
  user: 'USER',
  host: 'foo.server.com',
  port: 22,
  // Key shouldnt required using an ssh agent
  // key: '~/.ssh/keys/open_ssh_rsa_webksde-tf_PRIVATE',
  remotePath: '/DIRECTORY',
  excludeFolders: ['node_modules'],
};
